use std::io::{self, Write};
use std::process::{self, Command};
use std::env;
use std::path::Path;

fn main() {
    loop {
        let mut input = String::new();
        print!("rush> ");
        io::stdout().flush().expect("flush error");
        match io::stdin().read_line(&mut input) {
            Ok(_) => {
                input.pop();
                let args : Vec<&str> = input.split_whitespace().collect();
                if args.len() == 0 {
                    continue;
                }
                match args[0] {
                    "exit" => {
                        println!("bye.");
                        process::exit(0);
                    }
                    "cd" => {
                        let pathname;
                        if args.len() == 1 {
                            match env::var("HOME") {
                                Ok(home) => {
                                    pathname = home;
                                }
                                Err(e) => {
                                    println!("error: {}", e);
                                    continue;
                                }
                            }
                        } else {
                            pathname = args[1].to_string();
                        }
                        let path = Path::new(&pathname);
                        match env::set_current_dir(&path) {
                            Ok(_) => { ; }
                            Err(e) => println!("error: {}", e),
                        }
                    }
                    _ => {
                        match Command::new(args[0]).args(&args[1..]).status() {
                            Ok(_) => {
                                ;
                            }
                            Err(e) => println!("error: {}", e),
                        }
                        
                    }
                }
            }
            Err(e) => println!("error: {}", e),
        }
    }
}
